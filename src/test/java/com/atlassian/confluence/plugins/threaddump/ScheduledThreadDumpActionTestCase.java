package com.atlassian.confluence.plugins.threaddump;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import junit.framework.TestCase;

public class ScheduledThreadDumpActionTestCase extends TestCase
{
    private ScheduledThreadDumpAction scheduledThreadDumpAction;
    
    @Mock private ThreadDumpBuilder threadDumpBuilder;
    
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        scheduledThreadDumpAction = new ScheduledThreadDumpAction();
        scheduledThreadDumpAction.setThreadDumpBuilder(threadDumpBuilder);
    }

    public void testExecuteThreadDumpAction() throws Exception
    {
        int count = 10;
        int intervalSeconds = 350;
        
        scheduledThreadDumpAction.setCount(count);
        scheduledThreadDumpAction.setIntervalSeconds(intervalSeconds);
        
        assertEquals("success", scheduledThreadDumpAction.execute());
        
        assertEquals(count, scheduledThreadDumpAction.getCount());
        assertEquals(intervalSeconds, scheduledThreadDumpAction.getIntervalSeconds());
    }
}
