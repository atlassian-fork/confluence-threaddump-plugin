package com.atlassian.confluence.plugins.threaddump;

import com.atlassian.event.api.EventPublisher;
import junit.framework.TestCase;
import org.mockito.MockitoAnnotations;
import org.mockito.Mock;
import static org.mockito.Mockito.*;


public class SupportRequestCreatedEventListenerTestCase extends TestCase
{

    @Mock private EventPublisher eventPublisher;

    @Mock private ThreadDumpBuilder threadDumpBuilder;

    private SupportRequestCreatedEventListener supportRequestCreatedEventListener;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
         MockitoAnnotations.initMocks(this);
        supportRequestCreatedEventListener = new SupportRequestCreatedEventListener();


    }

    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    public void testEventRegistration() throws Exception
    {
        supportRequestCreatedEventListener.setEventPublisher(eventPublisher);
        verify(eventPublisher).register(isA(SupportRequestCreatedEventListener.class));
    }

    public void testThreadDumpCalled() throws Exception
    {
        supportRequestCreatedEventListener.setThreadDumpBuilder(threadDumpBuilder);
        supportRequestCreatedEventListener.generateThreadDump(null); // it does not make use of the event, so no need to create one
        verify(threadDumpBuilder, atMost(1)).build();

    }




}
