package it.com.atlassian.plugins.threaddump;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;

public class ThreadDumpTestCase extends AbstractConfluencePluginWebTestCase
{
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        gotoPageWithEscalatedPrivileges("/admin/console.action");
        clickLinkWithText("Thread Dump");
    }

    @Override
    protected void tearDown() throws Exception
    {
        dropEscalatedPrivileges();
        super.tearDown();
    }

    public void testAssertThreadDumpPageUserInterface()
    {
        assertEquals("Generate a Thread Dump Immediately",
                getElementTextByXPath("//form[@name='threaddumpform']//fieldset//h2"));
        
        assertElementPresentByXPath("//form[@name='threaddumpform']//div[@class='submit']//input[@value='Generate Now']");
        
        assertEquals("Schedule Thread Dump Execution",
                getElementTextByXPath("//form[@name='schedulethreaddump']//fieldset//h2"));
        
        assertElementPresentByXPath("//form[@name='schedulethreaddump']//input[@id='count']");
        assertElementPresentByXPath("//form[@name='schedulethreaddump']//input[@id='intervalSeconds']");
        
        assertElementPresentByXPath("//form[@name='schedulethreaddump']//input[@value='Schedule Now']");
    }
    
    public void testClickGenerateNowButtonResult()
    {
        clickElementByXPath("//form[@name='threaddumpform']//div[@class='submit']//input[@value='Generate Now']");
        
        assertEquals("Thread Dump Output",
                getElementTextByXPath("//form[@name='threaddumpform']//fieldset[2]//h3"));
        assertElementPresentByXPath("//form[@name='threaddumpform']//textarea");
    }
    
    public void testClickScheduleNowButtonResult()
    {
        setWorkingForm("schedulethreaddump");
        
        setTextField("count", "2");
        
        setTextField("intervalSeconds", "20");
        
        clickElementByXPath("//form[@name='schedulethreaddump']//input[@value='Schedule Now']");
        
        assertEquals("There will be 2 thread dumps taken 20 seconds apart. Please check your logs to gather the thread dumps.",
                getElementTextByXPath("//p[@class='thread-dump-scheduled']"));
        
        assertLinkPresentWithText("<< Back to Generate Thread Dumps");
    }
}
