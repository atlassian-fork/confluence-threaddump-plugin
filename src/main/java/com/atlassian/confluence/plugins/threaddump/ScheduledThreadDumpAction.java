package com.atlassian.confluence.plugins.threaddump;

import com.atlassian.confluence.core.ConfluenceActionSupport;

import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import org.apache.log4j.Logger;

/**
 * Periodically generates a thread dump every few configured interval and logs it.
 */
public class ScheduledThreadDumpAction extends ConfluenceActionSupport
{
    private static final Logger logger = Logger.getLogger(ScheduledThreadDumpAction.class);
    private static final int MAX_COUNT = 10;
    private static final int MAX_INTERVAL_SECONDS = 600;

    private int count;
    private int intervalSeconds;
    private ThreadDumpBuilder builder;

    @Override
    public boolean isPermitted()
    {
        return permissionManager.hasPermission(getRemoteUser(), Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM);
    }

    public void validate()
    {
        super.validate();
        if (count <= 0 || count > MAX_COUNT)
        {
            addFieldError("count", "threaddump.validation.error.count",
                new Object[] { MAX_COUNT });
        }
        if (intervalSeconds <= 0 || intervalSeconds > MAX_INTERVAL_SECONDS)
        {
            addFieldError("intervalSeconds", "threaddump.validation.error.intervalSeconds",
                new Object[] { MAX_INTERVAL_SECONDS });
        }
    }

    public int getIntervalSeconds()
    {
        return intervalSeconds;
    }

    public void setIntervalSeconds(int intervalSeconds)
    {
        this.intervalSeconds = intervalSeconds;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public String execute() throws Exception
    {
        Runnable r = new Runnable()
        {

            public void run()
            {
                for (int i = 0; i < count; i++)
                {
                    logger.warn(builder.build());
                    try
                    {
                        Thread.sleep(intervalSeconds * 1000);
                    }
                    catch (InterruptedException e)
                    {
                        throw new RuntimeException(e);
                    }
                }
            }
        };
        new Thread(r).start();
        return super.execute();
    }

    public void setThreadDumpBuilder(ThreadDumpBuilder builder)
    {
        this.builder = builder;
    }
}
