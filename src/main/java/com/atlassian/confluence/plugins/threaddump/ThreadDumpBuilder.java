package com.atlassian.confluence.plugins.threaddump;

import com.atlassian.confluence.util.GeneralUtil;

import java.util.Map;
import java.util.Date;
import java.util.Set;
import java.util.Map.Entry;
import java.text.MessageFormat;

public class ThreadDumpBuilder
{
    /**
     * Returns a thread dump of the currently running thread in a JVM.
     */
    public String build()
    {
        StringBuilder output = new StringBuilder(1000);
        appendPrefix(output);
        for (Map.Entry<Thread, StackTraceElement[]> stackTrace : getAllStackTracesEntrySet())
        {
            appendThreadStackTrace(output, stackTrace.getKey(), stackTrace.getValue());
        }
        return output.toString();
    }

    Set<Entry<Thread, StackTraceElement[]>> getAllStackTracesEntrySet()
    {
        return Thread.getAllStackTraces().entrySet();
    }

    private void appendThreadStackTrace(StringBuilder output, Thread thread, StackTraceElement[] stack)
    {
        if (thread.equals(getCurrentThread()))
            return; // filter out current thread

        output.append(thread).append("\n");
        for (StackTraceElement element : stack)
        {
            output.append("\t").append(element).append("\n");
        }
    }

    Thread getCurrentThread()
    {
        return Thread.currentThread();
    }

    private void appendPrefix(StringBuilder output)
    {
        output.append(MessageFormat.format(
            "Confluence {0} thread dump taken on {1,date,medium} at {1,time,medium}:\n",
            GeneralUtil.getVersionNumber(), new Date()));
    }
}
